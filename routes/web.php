<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UlasanController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::view('/data-table', 'data-table');
Route::view('/table', 'table');

Route::middleware(['auth'])->group(function () {
  // route untuk mengarah ke form tambah cast
  Route::get('/cast/create', [CastController::class, 'create'])->name('cast.create');
  // route untuk menyimpan data inputan ke db
  Route::post('/cast', [CastController::class, 'store'])->name('cast.store');
  // menampilkan semua data yang di db
  Route::get('/cast', [CastController::class, 'index'])->name('cast.index');
  // detail cast berdasarkan id
  Route::get('/cast/{cast_id}', [CastController::class, 'show'])->name('cast.show');
  // untuk mengarah ke halaman form update berdasarkan id
  Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit'])->name('cast.edit');
  // untuk update data
  Route::put('/cast/{cast_id}', [CastController::class, 'update'])->name('cast.update');
  // menghapus data
  Route::delete('/cast/{cast_id}', [CastController::class, 'destroy'])->name('cast.destroy');

  Route::resource('profile', ProfileController::class)->only('index', 'update');
  Route::resource('genre', GenreController::class);

  Route::post('/ulasan/{berita_id}', [UlasanController::class, 'store']);
});


Route::resource('film', FilmController::class);

Auth::routes();
