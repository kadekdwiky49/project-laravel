<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $iduser = Auth::id();

        $profile = Profile::where('users_id', $iduser)->first();

        return view('profile.update', ['profile' => $profile]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'umur' => ['required'],
            'alamat' => ['required'],
            'bio' => ['required'],
        ]);

        $profile = Profile::find($id);

        $profile->umur = $request->umur;
        $profile->alamat = $request->alamat;
        $profile->bio = $request->bio;

        $profile->save();

        return redirect('/profile');
    }
}
