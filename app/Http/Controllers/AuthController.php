<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Psy\CodeCleaner\FunctionReturnInWriteContextPass;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        return view('welcome', [
            'namaDepan' => $request['fname'],
            'namaBelakang' => $request['lname'],
            'jenisKelamin' => $request['gender']
        ]);
    }
}
