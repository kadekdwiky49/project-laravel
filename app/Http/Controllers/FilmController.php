<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use File;

class FilmController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();

        return view('film.index', ['film' => $film]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        return view('film.tambah', ['genre' => $genre]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => ['required'],
            'ringkasan' => ['required'],
            'tahun' => ['required'],
            'poster' => ['required', 'mimes:jpg,jpeg,png', 'max:2048'],
            'genre_id' => ['required']
        ]);

        // menggunakan unik name
        // $namaPoster = time() . '.' . $request->poster->extension();
        // Menggunakan Slug
        $namaPoster = Str::slug($request->judul, '-') . '.' . $request->poster->extension();

        $request->poster->move(public_path('image'), $namaPoster);

        $film = new Film;

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $namaPoster;
        $film->genre_id = $request->genre_id;

        $film->save();

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        return view('film.detail', ['film' => $film]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = Genre::all();
        $film = Film::find($id);
        return view('film.edit', [
            'film' => $film,
            'genre' => $genre
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => ['required'],
            'ringkasan' => ['required'],
            'tahun' => ['required'],
            'poster' => ['required', 'mimes:jpg,jpeg,png', 'max:2048'],
            'genre_id' => ['required']
        ]);

        $film = Film::find($id);

        if ($request->has('gambar')) {
            $path = 'image/';
            File::delete($path . $film->poster);

            $namaPoster = time() . '.' . $request->poster->extension();

            $request->poster->move(public_path('image'), $namaPoster);

            $film->poster = $namaPoster;

            $film->save();
        }

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $namaPoster;
        $film->genre_id = $request->genre_id;

        $film->save();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);

        $path = 'image/';
        File::delete($path . $film->poster);

        $film->delete();

        return redirect('/film');
    }
}
