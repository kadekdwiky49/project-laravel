<?php

namespace App\Http\Controllers;

use App\Models\Ulasan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UlasanController extends Controller
{
    public function store(Request $request, $id)
    {
        $request->validate([
            'konten' => ['required'],
            'point' => ['required'],
        ]);

        $ulasan = new Ulasan;

        $iduser = Auth::id();

        $ulasan->konten = $request->konten;
        $ulasan->point = $request->point;
        $ulasan->users_id = $iduser;
        $ulasan->film_id = $id;

        $ulasan->save();

        return redirect('/film/' . $request->film_id);
    }
}
