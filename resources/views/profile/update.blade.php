@extends('layouts.master')

@section('title')
    Halaman Update Profile
@endsection

@section('content')
<form method="post" action="/profile/{{ $profile->id }}">
  @csrf
  @method('put') 
  <div class="form-group">
    <label for="name">Nama user</label>
    <input type="text" class="form-control" disabled name="name" id="name" value="{{ $profile->user->name }}">
  </div>
  <div class="form-group">
    <label for="email">email user</label>
    <input type="text" class="form-control" disabled name="email" id="email" value="{{ $profile->user->email }}">
  </div>
  <div class="form-group">
    <label for="umur">Umur</label>
    <input type="number" class="form-control" name="umur" id="umur" value="{{ old('umur', $profile->umur) }}">
  </div>
  @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="alamat">Alamat</label>
    <textarea class="form-control" name="alamat" id="alamat" cols="30" rows="10">{{ old('alamat', $profile->alamat) }}</textarea>
  </div>
  @error('alamat')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="bio">Bio</label>
    <textarea class="form-control" name="bio" id="bio" cols="30" rows="10">{{ old('bio', $profile->bio) }}</textarea>
  </div>
  @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection