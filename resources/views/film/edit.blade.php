@extends('layouts.master')

@section('title')
    Halaman Edit Film
@endsection

@section('content')
<form method="post" action="{{ route('film.update') }}" enctype="multipart/form-data">
  @csrf
  @method('put')
  <div class="form-group">
    <label for="judul">Judul Film</label>
    <input type="text" class="form-control" name="judul" id="judul" value="{{ old('judul', $film->judul) }}">
  </div>
  @error('judul')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="tahun">Tahun Film</label>
    <input type="text" class="form-control" name="tahun" id="tahun"value="{{ old('tahun', $film->tahun) }}">
  </div>
  @error('tahun')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="genre_id">Genre Film</label>
    <select name="genre_id" id="genre_id" class="form-control">
      <option value="">---Pilih Genre---</option>
      @forelse ($genre as $value)
        @if ($value->id === $film->genre_id)
        <option value="{{ $value->id }} " selected>{{ $value->nama }}</option>
        @else 
        <option value="{{ $value->id }}">{{ $value->nama }}</option>
        @endif

      @empty
          Tidak Ada Genre
      @endforelse
    </select>
  </div>
  @error('genre_id')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="ringkasan">Ringkasan Film</label>
    <textarea class="form-control" cols="30" rows="10" name="ringkasan" id="ringkasan">{{ old('ringkasan') }}</textarea>  
  </div>
  @error('ringkasan')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="poster">Poster Film</label>
    <input type="file" class="form-control" name="poster" id="poster">
  </div>
  @error('poster')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection