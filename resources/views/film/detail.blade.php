@extends('layouts.master')

@section('title')
    Halaman Detail Film
@endsection

@push('scripts')
    <script src="https://cdn.tiny.cloud/1/32f7rj0gbe0nv9nemz12w2i7ilovqx57pagff68g13po1bvb/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'advlist autolink lists link image charmap preview anchor pagebreak',
            toolbar_mode: 'floating',
        });
    </script>
@endpush

@section('content')

<img src="{{ asset('/image/'.$film->poster) }}" height="200px" class="card-img-top" alt="...">
<div class="card-body">
<h2>{{ $film->judul }}</h2>
<p class="card-text my-4">{{ $film->ringkasan }}</p>

<h2>List Film</h2>
@forelse ($film->ulasan as $value)
<div class="media">
    <h1 class="text-primary mr-3">{{ $value->point }}</h1>
    <div class="media-body">
      <h5 class="mt-0">{{ $value->user->name }} ({{ $value->user->profile->umur }})</h5>
      <p>{!!$value->konten!!}</p>
    </div>
  </div>
@empty
    <h1>Tidak ada komentar</h1>
@endforelse




<form action="/ulasan/{{ $film->id }}" method="post">
@csrf
<select name="point" id="point" class="form-control my-2">
    <option value="">--Kasih Point--</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
</select>
<textarea name="konten" id="konten" cols="30" rows="10" class="form-control my-2" placeholder="Berikan komentar.."></textarea>

<input type="submit" value="Tambah" class="btn btn-info btn-sm btn-block my-2">
</form>

<a href="{{ route('film.index') }}" class="btn btn-secondary btn-sm">Kembali</a>

@endsection