@extends('layouts.master')

@section('title')
    Halaman List Film
@endsection

@section('content')
@auth
<a href="{{ route('film.create') }}" class="btn btn-primary btn-sm my-3">Tambah Film</a>
@endauth
<div class="row">
  @foreach ($film as $value)
  <div class="col-4">
    <div class="card" style="width: 18rem;">
      <img src="{{ asset('/image/'.$value->poster) }}" height="200px" class="card-img-top" alt="...">
      <div class="card-body">
        <h2>{{ $value->judul }}</h2>
        <p class="card-text">{{ Str::limit($value->ringkasan, 30) }}</p>
        <span class="badge badge-info">{{ $value->genre->nama }}</span>
        <div class="row">
          <div class="col">
            <a href="/film/{{ $value->id }}" class="btn btn-primary btn-block btn-sm">Detail</a>
          </div>
          @auth
          <div class="col">
            <a href="/film/{{ $value->id }}/edit" class="btn btn-warning btn-block btn-sm">Update</a>
          </div>
          <div class="col">
            <form action="/film/{{ $value->id }}" method="post">
            @csrf
            @method('delete')
            <input type="submit" value="Delete" class="btn btn-danger btn-block btn-sm">
            </form>
          </div>
          @endauth
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>


@endsection