@extends('layouts.master')

@section('judul')
    Halaman Utama
@endsection

@section('content')
<h1>SELAMAT DATANG {{ $namaDepan }} {{ $namaBelakang }}</h1>

<h3>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h3>

<h4>Jenis Kelamin = 
  @if ($jenisKelamin === "1")
      Laki-Laki
  @else 
      Perempuan
  @endif
</h4>
@endsection