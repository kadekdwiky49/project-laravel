@extends('layouts.master')

@section('title')
    Halaman Detail Genre
@endsection
 
@section('content')

<h1 class="text-primary">{{ $genre->nama }}</h1>
<p>{{ $genre->deskripsi }}</p>

<div class="row">
    @forelse ($genre->film as $value)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img src="{{ asset('/image/'.$value->poster) }}" height="200px" class="card-img-top" alt="...">
            <div class="card-body">
              <h2>{{ $value->judul }}</h2>
              <p class="card-text">{{ Str::limit($value->ringkasan, 30) }}</p>
                <div class="row">
                     <div class="col">
                  <a href="/film/{{ $value->id }}" class="btn btn-primary btn-block btn-sm">Detail</a>
                     </div>
                 </div>
            </div>
          </div>
    </div>
        @empty
            <h1>Tidak ada berita</h1>
        @endforelse
</div>

<a href="{{ route('genre.index') }}" class="btn btn-primary btn-sm">Kembali</a>

@endsection