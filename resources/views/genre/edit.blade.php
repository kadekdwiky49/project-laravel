@extends('layouts.master')

@section('title')
    Halaman edit Genre
@endsection

@section('content')
<form method="post" action="/genre/{{ $genre->id }}">
  @csrf
  @method('put')
  <div class="form-group">
    <label for="nama">Nama genre</label>
    <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama', $genre->nama) }}">
  </div>
  @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="deskripsi">deskripsi</label>
    <input type="text" class="form-control" name="deskripsi" id="deskripsi" value="{{ old('deskripsi', $genre->deskripsi) }}">
  </div>
  @error('deskripsi')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection