@extends('layouts.master')

@section('title')
    Halaman Tampil Genre
@endsection

@section('content')

<a href="{{ route('genre.create') }}" class="btn btn-primary btn-sm my-3">Tambah Genre</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $value)
        <tr>
          <th scope="row">{{ $key + 1 }}</th>
          <td>{{ $value->nama }}</td>
          <td>
            <form action="{{ route('genre.destroy', $value->id) }}" method="post">
              @csrf
              @method('delete')
            <a href="{{ route('genre.show', $value->id) }}" class="btn btn-info btn-sm">Detail</a>
              <a href="{{ route('genre.edit', $value->id) }}" class="btn btn-warning btn-sm">Edit</a>
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
          </td>
        </tr>
        @empty
            <h1>Data Kosong</h1>
        @endforelse
    </tbody>
  </table>
@endsection