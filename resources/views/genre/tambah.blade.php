@extends('layouts.master')

@section('title')
    Halaman Tambah Genre
@endsection

@section('content')
<form method="post" action="{{ route('genre.store') }}">
  @csrf
  <div class="form-group">
    <label for="nama">Nama Genre</label>
    <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama') }}">
  </div>
  @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="deskripsi">Deskripsi</label>
    <input type="text" class="form-control" name="deskripsi" id="deskripsi"value="{{ old('deskripsi') }}">
  </div>
  @error('deskripsi')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection