@extends('layouts.master')

@section('title')
    Halaman Detail Cast
@endsection

@section('content')

<h2>{{ $cast->nama }}</h2>
<h5>berumur {{ $cast->umur }} tahun</h5>
<p>{{ $cast->bio }}</p>

<a href="{{ route('cast.index') }}" class="btn btn-primary btn-sm">Kembali</a>

@endsection