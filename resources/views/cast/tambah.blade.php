@extends('layouts.master')

@section('title')
    Halaman Tambah Cast
@endsection

@section('content')
<form method="post" action="{{ route('cast.store') }}">
  @csrf
  <div class="form-group">
    <label for="nama">Nama Cast</label>
    <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama') }}">
  </div>
  @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="umur">Umur</label>
    <input type="text" class="form-control" name="umur" id="umur"value="{{ old('umur') }}">
  </div>
  @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="bio">Bio</label>
    <textarea class="form-control" cols="30" rows="10" name="bio" id="bio">{{ old('bio') }}</textarea>  
  </div>
  @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection