@extends('layouts.master')

@section('title')
    Halaman edit Cast
@endsection

@section('content')
<form method="post" action="/cast/{{ $cast->id }}">
  @csrf
  @method('put')
  <div class="form-group">
    <label for="nama">Nama Cast</label>
    <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama', $cast->nama) }}">
  </div>
  @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="umur">Umur</label>
    <input type="text" class="form-control" name="umur" id="umur"value="{{ old('umur', $cast->umur) }}">
  </div>
  @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="bio">Bio</label>
    <textarea class="form-control" cols="30" rows="10" name="bio" id="bio">{{ old('bio', $cast->bio) }}</textarea>
  </div>
  @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection