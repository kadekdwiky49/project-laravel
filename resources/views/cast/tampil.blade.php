@extends('layouts.master')

@section('title')
    Halaman Tampil Cast
@endsection

@section('content')

<a href="{{ route('cast.create') }}" class="btn btn-primary btn-sm my-3">Tambah Cast</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
          <th scope="row">{{ $key + 1 }}</th>
          <td>{{ $item->nama }}</td>
          <td>
            <form action="{{ route('cast.destroy', $item->id) }}" method="post">
              @csrf
              @method('delete')
              <a href="{{ route('cast.show', $item->id) }}" class="btn btn-info btn-sm">Detail</a>
              <a href="{{ route('cast.edit', $item->id) }}" class="btn btn-warning btn-sm">Edit</a>
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
          </td>
        </tr>
        @empty
            <h1>Data Kosong</h1>
        @endforelse
    </tbody>
  </table>
@endsection