@extends('layouts.master')

@section('title')
    Halaman Biodata
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="post">
  @csrf
  <label for="fname">
    First Name :
  </label><br><br>
  <input type="text" name="fname" id="fname">

  <br><br>

  <label for="lname">
    Last Name :
  </label><br><br>
  <input type="text" name="lname" id="lname">


  <br><br>

  <label for="gender">
    Gender :
  </label><br>
  <input type="radio" name="gender" value="1">Male <br>
  <input type="radio" name="gender" value="2">Female <br>
  <input type="radio" name="gender" value="3">Other

  <br><br>

  <label for="">Nationality :</label> <br>
  <select name="nationality" id="nationality">
    <option value="indonesia">Indonesia</option>
    <option value="malaysia">Malaysia</option>
    <option value="singapore">Singapore</option>
    <option value="fhilipina">Fhilipina</option>
    <option value="rusia">Rusia</option>
  </select>
  <br><br>

  <label for="language">Language Spoken :</label> <br>
  <input type="checkbox" name="language">Bahasa Indonesia <br>
  <input type="checkbox" name="language">English <br>
  <input type="checkbox" name="language">Other <br>

  <br>

  <label for="bio">Bio :</label> <br><br>
  <textarea name="bio" id="bo" cols="30" rows="10"></textarea>
  <br><br>

  <input type="submit">

</form>
@endsection